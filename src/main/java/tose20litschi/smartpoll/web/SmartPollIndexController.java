package tose20litschi.smartpoll.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

import java.util.Collection;
import java.util.HashMap;

/**
 * WWW-Controller
 * Liefert unter "/" die Startsite Seite
 *
 * @author F. Lanzeray / J. Lüthi
 */
public class SmartPollIndexController implements TemplateViewRoute {

	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(SmartPollIndexController.class);

	/**
	 * Liefert die Root-Seite "/" zurück
	 */
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		HashMap<String, String> model = new HashMap<>();
		return new ModelAndView(model, "index");
	}
}
